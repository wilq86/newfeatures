﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace NewFeatures.v7_0
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-7#out-variables
    /// </summary>
    [TestFixture]
    public class OutVariables
    {
        [Test]
        public void ShouldTestOutVariables()
        {
            if (!double.TryParse("7.0", NumberStyles.Any, CultureInfo.InvariantCulture, out double result))
            {
                Assert.Fail();
            }

            if (!double.TryParse("7.0", NumberStyles.Any, CultureInfo.InvariantCulture, out var answer))
            {
                Assert.Fail();
            }
        }
    }
}
