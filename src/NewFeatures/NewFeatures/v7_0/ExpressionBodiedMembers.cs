﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewFeatures.v7_0
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-7#more-expression-bodied-members
    /// </summary>
    [TestFixture]
    public class ExpressionBodiedMembers
    {
        // Expression-bodied constructor
        public ExpressionBodiedMembers() => this.Label = "Label";

        // Expression-bodied finalizer
        ~ExpressionBodiedMembers() => Console.Error.WriteLine("Finalized!");

        private string label;

        // Expression-bodied get / set accessors.
        public string Label
        {
            get => label;
            set => this.label = value ?? "Default label";
        }

        [Test]
        public void ShouldUseLambda()
        {
            Assert.AreEqual("Label", Label);
            Label = null;
            Assert.AreEqual("Default label", Label);
        }
    }
}
