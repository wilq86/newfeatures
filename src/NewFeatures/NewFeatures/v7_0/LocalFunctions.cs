﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.v7_0
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-7#local-functions
    /// </summary>
    [TestFixture]
    public class LocalFunctions
    {
        public Task<string> PerformLongRunningWork(string address, string name)
        {
            if (string.IsNullOrWhiteSpace(address))
                throw new ArgumentException(message: "An address is required", paramName: nameof(address));

            return longRunningWorkImplementation();

            async Task<string> longRunningWorkImplementation()
            {
                return await FirstWork(name);
            }
        }
        private Task<string> FirstWork(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException(message: "You must supply a name", paramName: nameof(name));

            return Task.Run(() => name);
        }

        [Test]
        public void ShouldThrowsException()
        {
            Assert.Throws<ArgumentException>(() => PerformLongRunningWork(null, "name"));
        }

        [Test]
        public void ShouldNotThrowsException()
        {
            Assert.DoesNotThrow(() => PerformLongRunningWork("address", null));
        }
    }
}
