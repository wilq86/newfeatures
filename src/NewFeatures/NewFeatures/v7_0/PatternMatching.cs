﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewFeatures.v7_0
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-7#pattern-matching
    /// </summary>
    [TestFixture]
    public class PatternMatching
    {
        [Test]
        public void RhouldMatch()
        {
            object input1 = 10.0d;
            object input2 = 10;

            double sum = 0;

            if (input1 is double doubleValue)
            {
                sum += doubleValue;
            }

            if (input2 is int intValue)
            {
                sum += intValue;
            }

            Assert.AreEqual(20.0d, sum);
        }

        public double SumNumbersBetween0And1000(List<object> objects)
        {
            double sum = 0;

            foreach (var obj in objects)
            {
                switch (obj)
                {
                    case int n when n > 1000 || n < 0:
                    case float f when f > 1000 || f < 0:
                    case double d when d > 1000 || d < 0:
                    case decimal m when m > 1000 || m < 0:
                        {
                            throw new ArgumentException("Value should be between 0 and 1000");
                        }
                    case int n:
                        {
                            sum += n;
                            break;
                        }
                    case float n:
                        {
                            sum += n;
                            break;
                        }
                    case double n:
                        {
                            sum += n;
                            break;
                        }
                    case decimal n when n > 1000 || n < 0:
                        {
                            sum += (double)n;
                            break;
                        }
                    case decimal n:
                        {
                            sum += (double)n;
                            break;
                        }
                    default:
                        {
                            throw new NotImplementedException($"Conversion of ${obj.GetType().FullName} not implemented");
                        }
                }
            }
            return sum;
        }

        [Test]
        public void ShouldSumNumbersBetween0And1000()
        {
            List<object> objects = new List<object>()
            {
                1.0d,
                2.0f,
                3,
                4m
            };

            Assert.AreEqual(10, SumNumbersBetween0And1000(objects), 0.000001);
            Assert.Throws<ArgumentException>(() => SumNumbersBetween0And1000(new List<object>() { -1 }));
        }
    }
}
