﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewFeatures.v7_0
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-7#ref-locals-and-returns
    /// </summary>
    [TestFixture]
    public class RefLocalsAndReturns
    {

        public static ref int Find(int[,] matrix, Func<int, bool> predicate)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
                for (int j = 0; j < matrix.GetLength(1); j++)
                    if (predicate(matrix[i, j]))
                        return ref matrix[i, j];
            throw new InvalidOperationException("Not found");
        }

        [Test]
        public void ShouldChangeTheValue()
        {
            var matrix = new int[2, 2] { { 10, 11 }, { 15, 42 } };
            ref var item = ref Find(matrix, (val) => val == 42);

            Assert.AreEqual(42, item);
            Assert.AreEqual(42, matrix[1, 1]);

            item = 24;

            Assert.AreEqual(24, item);
            Assert.AreEqual(24, matrix[1, 1]);
        }
    }
}
