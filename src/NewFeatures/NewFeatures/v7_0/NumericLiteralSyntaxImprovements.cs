﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewFeatures.v7_0
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-7#numeric-literal-syntax-improvements
    /// </summary>
    [TestFixture]
    public class NumericLiteralSyntaxImprovements
    {
        public const int Sixteen = 0b0001_0000;
        public const int ThirtyTwo = 0b0010_0000;
        public const int SixtyFour = 0b0100_0000;
        public const int OneHundredTwentyEight = 0b1000_0000;

        public const long BillionsAndBillions = 100_000_000_000;

        public const double AvogadroConstant = 6.022_140_857_747_474e23;
        public const decimal GoldenRatio = 1.618_033_988_749_894_848_204_586_834_365_638_117_720_309_179M;

        [Test]
        public void ShouldBeValidValues()
        {
            Assert.AreEqual(16, Sixteen);
            Assert.AreEqual(32, ThirtyTwo);
            Assert.AreEqual(64, SixtyFour);
            Assert.AreEqual(128, OneHundredTwentyEight);

            Assert.AreEqual(100000000000, BillionsAndBillions);

            Assert.AreEqual(6.022_140_857_747_474e23, AvogadroConstant);
            Assert.AreEqual(1.618_033_988_749_894_848_204_586_834_365_638_117_720_309_179M, GoldenRatio);
        }
    }
}
