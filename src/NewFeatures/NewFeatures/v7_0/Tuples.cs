﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewFeatures.v7_0
{
    /// <summary>
    /// Example of Deconstruct Method
    /// </summary>
    public class Point
    {
        public Point(double x, double y)
            => (X, Y) = (x, y);

        public double X { get; }
        public double Y { get; }

        public void Deconstruct(out double x, out double y) =>
            (x, y) = (X, Y);
    }

    /// <summary>
    /// 
    /// </summary>
    [TestFixture]
    public class Tuples
    {
        [Test]
        public void ShouldTestTuplesFeature()
        {
            (string Alpha, string Beta) namedLetters = ("a", "b");

            Assert.AreEqual("a", namedLetters.Alpha);
            Assert.AreEqual("b", namedLetters.Beta);

            var alphabetStart = (Alpha: "a", Beta: "b");
            Assert.AreEqual("a", alphabetStart.Alpha);
            Assert.AreEqual("b", alphabetStart.Beta);


            (string alpha, string beta) = alphabetStart;
            Assert.AreEqual("a", alpha);
            Assert.AreEqual("b", beta);

            var p = new Point(3.14, 2.71);
            (double X, double Y) = p;

            Assert.AreEqual(3.14, X);
            Assert.AreEqual(2.71, Y);

            //Discards
            (double X2, _) = p;
            Assert.AreEqual(3.14, X2);
        }
    }
}
