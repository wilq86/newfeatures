﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewFeatures.v7_0
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-7#throw-expressions
    /// </summary>
    [TestFixture]
    public class ThrowExpressions
    {
        private static void DisplayFirstNumber(string[] args)
        {
            string arg = args.Length >= 1 ? args[0] :
                                       throw new ArgumentException("You must supply an argument");

            if (Int64.TryParse(arg, out var number))
                Console.WriteLine($"You entered {number:F0}");
            else
                Console.WriteLine($"{arg} is not a number.");

        }

        private string name;

        public string Name
        {
            get => name;
            set => name = value ??
                throw new ArgumentNullException(paramName: nameof(value), message: "Name cannot be null");
        }

        DateTime ToDateTime(IFormatProvider provider) =>
         throw new InvalidCastException("Conversion to a DateTime is not supported.");

        [Test]
        public void ShouldThrowException()
        {
            Assert.Throws<InvalidCastException>(() => ToDateTime(null));
            Assert.Throws<ArgumentNullException>(() => Name = null);
            Assert.Throws<ArgumentException>(() => DisplayFirstNumber(new string[0]));
        }
    }
}
