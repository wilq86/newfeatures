﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewFeatures.v7_0
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-7#discards
    /// </summary>
    [TestFixture]
    public class Discards
    {
        [Test]
        public void ShouldTestDiscards()
        {
            var (name, _, _) = ReturnsTuple();

            (string, double, int) ReturnsTuple()
            {
                return ("First", 1, 1);
            };

            Assert.AreEqual(name, "First");
        }
    }
}
