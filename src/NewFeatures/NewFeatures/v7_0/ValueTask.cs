﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.v7_0
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-7#generalized-async-return-types
    /// </summary>
    class ValueTask
    {
        public async ValueTask<int> Func()
        {
            await Task.Delay(100);
            return 5;
        }
    }
}
