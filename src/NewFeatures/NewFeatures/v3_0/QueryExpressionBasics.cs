﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Net;

namespace NewFeatures.v3_0
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/csharp/linq/query-expression-basics
    /// </summary>
    [TestFixture]
    public class QueryExpressionBasics
    {
        [Test]
        public void shouldTestQuerySyntax()
        {
            List<int> scores = new List<int>() { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 };
            IEnumerable<int> highScoresQuery =
                from score in scores
                where score > 80
                orderby score descending
                select score;

            Assert.AreEqual(highScoresQuery.Count(), 2);
            Assert.AreEqual(highScoresQuery.ElementAt(0), 100);
            Assert.AreEqual(highScoresQuery.ElementAt(1), 90);
        }

        [Test]
        public void shouldTestMehtodSyntax()
        {
            List<int> scores = new List<int>() { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 };
            IEnumerable<int> highScoresQuery = scores.Where(x => x > 80).OrderByDescending(x => x);

            Assert.AreEqual(highScoresQuery.Count(), 2);
            Assert.AreEqual(highScoresQuery.ElementAt(0), 100);
            Assert.AreEqual(highScoresQuery.ElementAt(1), 90);
        }

        /// <summary>
        /// https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/join-clause
        /// </summary>
        [Test]
        public void shouldTestJoinSyntax()
        {
            List<string> keys = new List<string>()
            {
                "one",
                "two",
                "three",
                "four",
            };

            List<string> keys2 = new List<string>()
            {
                "one",
                "four",
            };

            var categoryQuery =
            from key in keys
            join key2 in keys2 on key equals key2 
            select new { Key1 = key, Key2 = key2 };

            Assert.AreEqual(categoryQuery.ElementAt(1).Key1, "four");
            Assert.AreEqual(categoryQuery.ElementAt(1).Key2, "four");
        }

        /// <summary>
        /// https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/let-clause
        /// </summary>
        [Test]
        public void shouldTestLetClause()
        {
            string[] strings =
                {
                    "A penny saved is a penny earned.",
                    "The early bird catches the worm.",
                    "The pen is mightier than the sword."
                };

            // Split the sentence into an array of words
            // and select those whose first letter is a vowel.
            var earlyBirdQuery =
                from sentence in strings
                let words = sentence.Split(' ')
                from word in words
                let w = word.ToLower()
                where w[0] == 'a' || w[0] == 'e'
                    || w[0] == 'i' || w[0] == 'o'
                    || w[0] == 'u'
                select word;

            Assert.AreEqual(earlyBirdQuery.Count(), 6);

        }

        public class A
        {
            public string Field;
        }

        [Test]
        public void shouldTestCaptureOfOuterVariables()
        {
            A a = new A();
            a.Field = "Test";

            Func<string> func = () => a.Field;
            a.Field = "Nothing";

            Assert.AreEqual(a.Field, func.Invoke());
        }
    }
}
