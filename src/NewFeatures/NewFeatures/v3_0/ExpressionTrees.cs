﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace NewFeatures.v3_0
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/csharp/expression-trees
    /// </summary>
    [TestFixture]
    public class ExpressionTrees
    {
        /// <summary>
        /// https://docs.microsoft.com/en-us/dotnet/csharp/expression-classes
        /// </summary>
        [Test]
        public void shouldTestSimpleExpression()
        {
            // Addition is an add expression for "1 + 2"
            var one = Expression.Constant(1, typeof(int));
            var two = Expression.Constant(2, typeof(int));
            var addition = Expression.Add(one, two);

            Expression<Func<int>> le = Expression.Lambda<Func<int>>(addition);
            Func<int> compiledExpression = le.Compile();

            double result = compiledExpression();

            Assert.AreEqual(result, 3);
        }

        [Test]
        public void shouldTestCreatingConstatnExpresion()
        {
            var constant = Expression.Constant(24, typeof(int));

            Assert.AreEqual(constant.NodeType, ExpressionType.Constant);
            Assert.AreEqual(constant.Type, typeof(int));
            Assert.AreEqual(constant.Value, 24);
        }
    }
}
