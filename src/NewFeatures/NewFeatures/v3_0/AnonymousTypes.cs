﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewFeatures.v3_0
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/anonymous-types
    /// </summary>
    [TestFixture]
    public class AnonymousTypes
    {
        [Test]
        public void shouldTestAnonymousTypes()
        {
            var v = new { Amount = 108, Message = "Hello" };

            Assert.AreEqual(v.Amount, 108);
            Assert.AreEqual(v.Message, "Hello");

            List<string> list = new List<string>() { "Test", "Hello", "World", "!" };

            list.Select(x => new { Str = x, Length = x.Length });

        }
    }
}
