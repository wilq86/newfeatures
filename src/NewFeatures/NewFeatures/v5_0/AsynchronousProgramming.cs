﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewFeatures.v5_0
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/csharp/async
    /// https://medium.com/@deep_blue_day/long-story-short-async-await-best-practices-in-net-1f39d7d84050
    /// </summary>
    [TestFixture]
    public class AsynchronousProgramming
    {
        //TODO: add tests

        [Test]
        public async Task shouldRunInAnotherThread()
        {
            int threadId = Thread.CurrentThread.ManagedThreadId;

            int asyncThreadId2 = await GetThreadIdAsync();
            Assert.AreNotEqual(threadId, asyncThreadId2);
        }

        private async Task<int> GetThreadIdAsync()
        {
            return await Task.Run(() => Thread.CurrentThread.ManagedThreadId);
        }


        [Test]
        public async Task shouldThrowExceptionAsync()
        {
            string str = "";

            Task task = Task.Run(() => { throw new Exception(); });

            str += "Test";

            try
            {
                await task;
                Assert.Fail();
            }
            catch(Exception )
            {
            }

            Assert.AreEqual(str, "Test");
        }


        [Test]
        public async Task shouldLastLessThan3SecondsAsync()
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            Task task = Task.Run(() => LongRunningTask(1));
            Task task2 = Task.Run(() => LongRunningTask(1));
            Task task3 = Task.Run(() => LongRunningTask(1));

            await Task.WhenAll(task, task2, task3);
            stopwatch.Stop();

            Assert.IsTrue(stopwatch.Elapsed < TimeSpan.FromSeconds(3));
        }

        private void LongRunningTask(int seconds)
        {
            Thread.Sleep(TimeSpan.FromSeconds(seconds));
        }

        [Test]
        public async Task shouldCatchOnlyOneExceptionAsync()
        {
            Task task1 =  Task.Run(() => ThrowException(1));
            Task task2 = Task.Run(() => ThrowException(2));
            Task task3 = Task.Run(() => ThrowException(3));

            try
            {
                await Task.WhenAll(task1, task2, task3);
            }
            catch(Exception exc)
            {
                //Only one exception is catched
                Assert.AreEqual(exc.Message, "1");
            }

        }

        private void ThrowException(int number)
        {
            throw new Exception($"{number}");
        }

        [Test]
        public async Task shouldAggreagte3ExceptionAsync()
        {
            Task task1 = Task.Run(() => ThrowException(1));
            Task task2 = Task.Run(() => ThrowException(2));
            Task task3 = Task.Run(() => ThrowException(3));

            Task task = Task.WhenAll(task1, task2, task3);

            try
            {
                await task;
            }
            catch (Exception exc)
            {
                Assert.AreEqual(exc.Message, "1");
                Assert.AreEqual(task.Exception.InnerExceptions.Count, 3);
            }

        }
    }
}
