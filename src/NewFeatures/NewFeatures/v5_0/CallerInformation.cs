﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace NewFeatures.v5_0
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/caller-information
    /// </summary>
    [TestFixture]
    public class CallerInformation
    {

        [Test]
        public void shouldCheckCallerInformation()
        {
            (var memberName, var filePath, var lineNumber) = Method();

            Assert.AreEqual("shouldCheckCallerInformation", memberName);
            Assert.IsTrue(filePath.EndsWith("CallerInformation.cs"));
            Assert.AreEqual(19, lineNumber);
        }


        public (string, string, int) Method([CallerMemberName] string callerMemberName = null, [CallerFilePath] string callerFilePath = null, [CallerLineNumber] int callerLineNumber = -1)
        {
            return (callerMemberName, callerFilePath, callerLineNumber);
        }

    }
}
