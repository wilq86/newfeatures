﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewFeatures.v4_0
{
    [TestFixture]
    public class DynamicType
    {
        [Test]
        public void shouldTestDynamicType()
        {
            dynamic dyn = 1;
            object obj = 1;

            Assert.AreEqual("Int32", dyn.GetType().Name);
            Assert.AreEqual("Int32", obj.GetType().Name);

            dyn = dyn + 3;
            //obj = obj + 3;

            try
            {
                dyn.AnyOtherMethod();
                Assert.Fail();
            }
            catch(Exception)
            {

            }
        }
    }
}
