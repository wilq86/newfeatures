﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewFeatures.v4_0
{
    public class Base { }
    public class Derived : Base { }

    [TestFixture]
    public class CovarianceAndContravarianceInGenerics
    {
        [Test]
        public void shouldTestCovariance()
        {

            IEnumerable<Derived> d = new List<Derived>();
            IEnumerable<Base> b = d;
        }

        [Test]
        public void shouldTestContravariance()
        {
            Action<Base> b = (target) => { Console.WriteLine(target.GetType().Name); };
            Action<Derived> d = b;
            d(new Derived());
        }
    }
}
