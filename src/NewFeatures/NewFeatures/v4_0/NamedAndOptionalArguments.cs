﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewFeatures.v4_0
{
    [TestFixture]
    public class NamedAndOptionalArguments
    {
        [Test]
        public void shouldTestOptionalArguments()
        {
            var tuple = Mehtod();

            Assert.AreEqual(tuple.Item1, "None");
            Assert.AreEqual(tuple.Item2, int.MinValue);
            Assert.AreEqual(tuple.Item3, false);
        }

        [Test]
        public void shouldTestNamedArguments()
        {
            var tuple = Mehtod("Name", 1, false);

            Assert.AreEqual(tuple.Item1, "Name");
            Assert.AreEqual(tuple.Item2, 1);
            Assert.AreEqual(tuple.Item3, false);

            tuple = Mehtod(flag: false, value: 1, name: "Name");

            Assert.AreEqual(tuple.Item1, "Name");
            Assert.AreEqual(tuple.Item2, 1);
            Assert.AreEqual(tuple.Item3, false);
        }

        public Tuple<string, int, bool> Mehtod(string name = "None", int value = int.MinValue, bool flag = false)
        {
            return new Tuple<string, int, bool>(name, value, flag);
        }

    }
}
