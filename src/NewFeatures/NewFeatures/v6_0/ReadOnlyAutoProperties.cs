﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewFeatures.v6_0
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-6#read-only-auto-properties
    /// </summary>
    class ReadOnlyAutoProperties
    {
        public string FirstName { get; }
        public string LastName { get; }

        public ReadOnlyAutoProperties(string firstName, string lastName)
        {
            if (string.IsNullOrWhiteSpace(lastName))
                throw new ArgumentException(message: "Cannot be blank", paramName: nameof(lastName));

            FirstName = firstName;
            LastName = lastName;
        }

        public void ChangeFirstName(string firstName)
        {
            //Property or indexer 'ReadOnlyAutoProperties.FirstName' cannot be assigned to --it is read only
            //FirstName = firstName;
        }
    }

    [TestFixture]
    public class ReadOnlyAutoPropertiesTest
    {
        [Test]
        public void shouldInitializeProperies()
        {
            var readOnlyAutoProperties = new ReadOnlyAutoProperties("Krzysztof", "Kowalski");

            Assert.AreEqual("Krzysztof", readOnlyAutoProperties.FirstName);
            Assert.AreEqual("Kowalski", readOnlyAutoProperties.LastName);
        }

    }

}
