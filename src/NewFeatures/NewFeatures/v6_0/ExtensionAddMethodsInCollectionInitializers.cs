﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace NewFeatures.v6_0
{
    public static class StringExtensions
    {
        public static void Add(this IList<string> list, string str1, string str2)
        {
            list.Add(str1);
            list.Add(str2);
        }
    }

    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-6#extension-add-methods-in-collection-initializers
    /// </summary>
    [TestFixture]
    public class ExtensionAddMethodsInCollectionInitializers
    {
        [Test]
        public void shouldAddToCollection()
        {
            List<string> strings = new List<string>()
            {
                { "1", "2" }
            };

            Assert.AreEqual(strings[0], "1");
            Assert.AreEqual(strings[1], "2");
        }
    }
}
