﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewFeatures.v6_0
{
    [TestFixture]
    public class InitializeAssociativeCollectionsUsingIndexers
    {
        private Dictionary<int, string> messages = new Dictionary<int, string>
        {
            { 404, "Page not Found"},
            { 302, "Page moved, but left a forwarding address."},
            { 500, "The web server can't come out to play today."}
        };

        private Dictionary<int, string> webErrors = new Dictionary<int, string>
        {
            [404] = "Page not Found",
            [302] = "Page moved, but left a forwarding address.",
            [500] = "The web server can't come out to play today."
        };

        public void shouldTestInitialization()
        {
            Assert.AreEqual(messages[404], webErrors[404]);
            Assert.AreEqual(messages[302], webErrors[302]);
            Assert.AreEqual(messages[500], webErrors[500]);
        }
    }
}
