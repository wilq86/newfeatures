﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewFeatures.v6_0
{
    public class Person
    {
        public const string Unspecified = "Unspecified";

        public string FirstName { get; }

        public Person(string firstName)
        {
            FirstName = firstName;
        }
    }

    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-6#null-conditional-operators
    /// </summary>
    [TestFixture]
    public class NullConditionalOperators
    {
        private const string firstName = "Józek";

        private Person person = new Person(firstName);
        private Person nameless = new Person(null);
        private Person nullPerson = (Person)null;

        [Test]
        public void shouldCheckOperatorQuestionMark()
        {
            Assert.AreEqual(firstName, person?.FirstName);
            Assert.AreEqual(null, nameless?.FirstName);
            Assert.AreEqual(null, nullPerson?.FirstName);

            Assert.AreEqual(firstName, person != null ? person.FirstName : null);
            Assert.AreEqual(null, nameless != null ? nameless.FirstName : null);
            Assert.AreEqual(null, nullPerson != null ? nullPerson.FirstName : null);
        }

        [Test]
        public void shouldCheckOperatorDoubleQuestionMark()
        {
            Assert.AreEqual(firstName, person.FirstName ?? Person.Unspecified);
            Assert.AreEqual(Person.Unspecified, nameless.FirstName ?? Person.Unspecified);
            Assert.Throws<NullReferenceException>(() => { string value = nullPerson.FirstName ?? Person.Unspecified; });

            Assert.AreEqual(firstName, person.FirstName == null ? Person.Unspecified : person.FirstName);
            Assert.AreEqual(Person.Unspecified, nameless.FirstName == null ? Person.Unspecified : nameless.FirstName);
            Assert.Throws<NullReferenceException>(() => { string value = nullPerson.FirstName == null ? Person.Unspecified : nullPerson.FirstName; });
        }

        [Test]
        public void shouldCheckOperatorDoubleQuestionMarkAndQuestionMark()
        {
            Assert.AreEqual(firstName, person?.FirstName ?? Person.Unspecified);
            Assert.AreEqual(Person.Unspecified, nameless?.FirstName ?? Person.Unspecified);
            Assert.AreEqual(Person.Unspecified, nullPerson?.FirstName ?? Person.Unspecified);

            Assert.AreEqual(firstName, (person != null ? person.FirstName : null) == null ? Person.Unspecified : (person != null ? person.FirstName : null));
            Assert.AreEqual(Person.Unspecified, (nameless != null ? nameless.FirstName : null) == null ? Person.Unspecified : (nameless != null ? nameless.FirstName : null));
            Assert.AreEqual(Person.Unspecified, (nullPerson != null ? nullPerson.FirstName : null) == null ? Person.Unspecified : (nullPerson != null ? nullPerson.FirstName : null));
        }
    }
}
