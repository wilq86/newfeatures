﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewFeatures.v6_0
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-6#the-nameof-expression
    /// </summary>
    [TestFixture]
    public class NameofExpression
    {
        public string Member;

        [Test]
        public void shouldPasteNameOfVariable()
        {
            Assert.AreEqual("Member", nameof(Member));
        }
    }
}
