﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

/***************************************************************************************/
using static System.Math;

namespace NewFeatures.v6_0
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-6#using-static
    /// </summary>
    [TestFixture]
    public class UsingStatic
    {
        [Test]
        public void shouldUseMath()
        {
            Assert.AreEqual(13.0d, Round(12.6));
        }
    }
}
