﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewFeatures.v6_0
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-6#expression-bodied-function-members
    /// </summary>
    [TestFixture]
    public class ExpressionBodiedFunctionMembers
    {
        private string firstName;

        private string lastName;

        public override string ToString() => $"{lastName}, {firstName}";

        public string FullName => $"{firstName} {lastName}";

        [Test]
        public void shouldRunLambdaExpresion()
        {
            firstName = "Adam";
            lastName = "Kowalski";
            Assert.AreEqual("Kowalski, Adam", ToString());
            Assert.AreEqual("Adam Kowalski", FullName);
        }
    }
}
