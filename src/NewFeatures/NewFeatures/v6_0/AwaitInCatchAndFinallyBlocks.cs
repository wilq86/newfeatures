﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.v6_0
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-version-history#c-version-60
    /// </summary>
    [TestFixture]
    public class AwaitInCatchAndFinallyBlocks
    {
        [Test]
        public static async Task shouldAwait()
        {
            try
            {
                await Task.Delay(10);
                throw new Exception("Test");
            }
            catch (Exception)
            {
                await Task.Delay(10);
            }
            finally
            {
                await Task.Delay(100);
            }
        }
    }
}
