﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewFeatures.v6_0
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-6#string-interpolation
    /// </summary>
    [TestFixture]
    public class StringInterpolation
    {
        [Test]
        public void shouldConcateString()
        {
            var now = DateTime.Now;
            Assert.AreEqual(string.Format("{0} {1} {2}", 10, 11.5, now), $"{10} {11.5} {now}");
        }
    }
}
