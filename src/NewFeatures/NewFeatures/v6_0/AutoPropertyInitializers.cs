﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewFeatures.v6_0
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-6#auto-property-initializers
    /// </summary>
    [TestFixture]
    public class AutoPropertyInitializers
    {
        public List<double> Grades { get; } = new List<double>() { 1, 2, 3 };

        [Test]
        public void ShouldBeInitilized()
        {
            Assert.AreEqual(3, Grades.Count);
            Assert.AreEqual(1, Grades[0]);
            Assert.AreEqual(2, Grades[1]);
            Assert.AreEqual(3, Grades[2]);
        }
    }
}
