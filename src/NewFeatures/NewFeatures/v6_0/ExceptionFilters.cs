﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.v6_0
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-6#exception-filters
    /// </summary>
    [TestFixture]
    public class ExceptionFilters
    {

        [Test]
        public void MakeRequest()
        {
            try
            {
                int b = 0;
                int a = 10 / b;
            }
            catch (Exception exc) when (exc.Message.Contains("Attempted to divide by zero."))
            {

            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }
    }
}
