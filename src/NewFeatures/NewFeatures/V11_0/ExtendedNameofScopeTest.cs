﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V11_0
{
    [TestFixture]
    public class ExtendedNameofScopeTest
    {
        public class NameAttribute : Attribute
        {
            public string ParameterName { get; }

            public NameAttribute(string parameterName)
            {
                ParameterName = parameterName;
            }
        }

        [Name(nameof(nameOfTheParameter))]
        public void Method(string nameOfTheParameter)
        {

        }

        [Test]
        public void ExtendedNameofScope()
        {
            MethodBase method = GetType().GetMethod("Method");
            NameAttribute attr = (NameAttribute)method.GetCustomAttributes(typeof(NameAttribute), true)[0];

            Assert.AreEqual("nameOfTheParameter", attr.ParameterName);
        }
    }
}
