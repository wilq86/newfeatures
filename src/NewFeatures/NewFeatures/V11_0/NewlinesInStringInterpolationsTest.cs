﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V11_0
{
    [TestFixture]
    public class NewlinesInStringInterpolationsTest
    {
        [Test]
        public void NewlinesInStringInterpolations()
        {
            string str = $"{
                1
                    +
                        5
                }";

            Assert.AreEqual("6", str);
        }
    }
}
