﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V11_0
{
    internal class ImprovedMethodGroupConversionToDelegateTest
    {
        delegate string D1(object o);
        delegate object D2(string s);
        delegate object D3();
        delegate string D4(object o, params object[] a);
        delegate string D5(int i);
        class Test
        {
            static string F(object o) { return "F"; }

            static void G()
            {
                D1 d1 = F;         // Ok
                D2 d2 = F;         // Ok

                // Error – not applicable
                // D3 d3 = F;         

                // Error – not applicable in normal form
                //D4 d4 = F;         

                // Error – applicable but not compatible
                //D5 d5 = F;         
            }
        }
    }
}
