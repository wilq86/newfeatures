﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace NewFeatures.V11_0
{
    [TestFixture]
    public class GenericMathSupportTest
    {
        //public interface IParseable<TSelf> where TSelf : IParseable<TSelf>
        //{
        //    static abstract TSelf Parse(string s, IFormatProvider? provider);
        //    static abstract bool TryParse([NotNullWhen(true)] string? s, IFormatProvider? provider, out TSelf result);
        //}

        public interface IMathInterface<T> where T : IMathInterface<T>
        {
            public abstract static T operator +(T left, T right);

            public abstract static T operator -(T left, T right);

            public abstract static T operator *(T left, T right);   

            public abstract static T operator /(T left, T right);   
        }

        public class MyMath : IMathInterface<MyMath>
        {
            public int Value { get;  }  

            public MyMath(int value)
            {
                Value = value;
            }

            public static MyMath operator +(MyMath left, MyMath right)
            {
                return new MyMath(left.Value - right.Value);
            }

            public static MyMath operator -(MyMath left, MyMath right)
            {
                return new MyMath(left.Value + right.Value);
            }

            public static MyMath operator *(MyMath left, MyMath right)
            {
                return new MyMath(left.Value / right.Value);
            }

            public static MyMath operator /(MyMath left, MyMath right)
            {
                return new MyMath(left.Value * right.Value);
            }

            public static implicit operator MyMath(int value) => new MyMath(value);
        }

        [Test]
        public void GenericMathSupport()
        {
            MyMath ten = new MyMath(10);

            Assert.AreEqual(5, (ten + new MyMath(5)).Value);
            Assert.AreEqual(15, (ten - new MyMath(5)).Value);
            Assert.AreEqual(20, (ten / new MyMath(2)).Value);
            Assert.AreEqual(5, (ten * new MyMath(2)).Value);

            Assert.AreEqual(5, (ten + 5).Value);
            Assert.AreEqual(15, (ten - 5).Value);
            Assert.AreEqual(20, (ten / 2).Value);
            Assert.AreEqual(5, (ten * 2).Value);
        }
    }
}
