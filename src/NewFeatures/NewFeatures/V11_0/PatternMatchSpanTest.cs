﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V11_0
{
    [TestFixture]
    public class PatternMatchSpanTest
    {
        [Test]
        public void PatternMatchSpan()
        {
            Span<char> span = new Span<char>("Test".ToArray());

            Assert.IsTrue(span is ['T', ..]);
            Assert.IsTrue(span is [.., 't']);
        }
    }
}
