﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V11_0
{
    [TestFixture]
    public class UTF8SstringLiteralsTest
    {
        [Test]
        public void UTF8SstringLiterals()
        {
            ReadOnlySpan<byte> u8 = "Jakiś tekst"u8;
            ReadOnlySpan<byte> u16 = Encoding.Unicode.GetBytes("Jakiś tekst");

            Assert.AreEqual(12, u8.Length);
            Assert.AreEqual(22, u16.Length);
        }
    }
}
