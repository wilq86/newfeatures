﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V11_0
{
    [TestFixture]
    public class GenericAttributesTest
    {
        public class GenericAttribute<T> : Attribute { }

        [Test]
        [GenericAttribute<string>()]
        public void GenericAttributes()
        {

        }
    }
}
