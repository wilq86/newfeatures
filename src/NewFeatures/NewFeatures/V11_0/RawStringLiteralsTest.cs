﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V11_0
{
    [TestFixture]
    public class RawStringLiteralsTest
    {

        [Test]
        public void RawStringLiterals()
        {
            string longMessage = 
                """
                    This is a long message.
                    It has several lines.
                        Some are indented
                                more than others.
                    Some should start at the first column.
                    Some have "quoted text" in them.
                 """;

            Assert.IsTrue(longMessage.StartsWith("   "));

            var location = $$"""
                            You are at {{0}}, {{0}}
                            """;

            Assert.AreEqual(location,"You are at 0, 0");
        }
    }
}
