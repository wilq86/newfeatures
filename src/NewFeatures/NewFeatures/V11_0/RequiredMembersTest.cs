﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V11_0
{
    [TestFixture]
    public class RequiredMembersTest
    {
        public class Person
        {
            public Person() { }

            //[SetsRequiredMembers]
            //public Person(string firstName, string lastName) =>
            //    (FirstName, LastName) = (firstName, lastName);

            public required string FirstName { get; init; }
            public required string LastName { get; init; }

            public int? Age { get; set; }
        }

        [Test]
        public void RequiredMembers()
        {
            //Compilation error FirstName and LastName must by set in
            //Person person = new Person();

            Person person = new Person()
            {
                FirstName = "Piotr",
                LastName = "Wilk"
            };

            Assert.AreEqual("Piotr", person.FirstName);
            Assert.AreEqual("Wilk", person.LastName);
        }
    }
}
