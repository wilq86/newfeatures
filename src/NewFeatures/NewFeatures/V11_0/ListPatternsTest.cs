﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V11_0
{
    [TestFixture]
    public class ListPatternsTest
    {
        [Test]
        public void ListPatterns()
        {
            int[] ints = new int[] { 1, 2, 3, 4, 5, 6, 7 };

            Assert.IsTrue(ints is [1, 2, _, ..]);
            Assert.IsTrue(ints is [1, _, 3, ..]);
            Assert.IsFalse(ints is [1, _, 4, ..]);

            Assert.IsTrue(ints[^3..] is [5, 6, 7]);
            Assert.IsTrue(ints[^3..] is [5, _, _]);
        }
    }
}
