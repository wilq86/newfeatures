﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V9_0
{
    /// <summary>
    /// https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/attributes/general#moduleinitializer-attribute
    /// </summary>
    [TestFixture]
    public class ModuleInitializerTest
    {
        public static string? Text { get; set; }

        [ModuleInitializer]
        public static void Init1()
        {
            Text += "Hello from Init1! ";
        }

        [ModuleInitializer]
        public static void Init2()
        {
            Text += "Hello from Init2! ";
        }

        [Test]
        public void ModuleInitializer()
        {
            Assert.AreEqual("Hello from Init1! Hello from Init2! ", Text);
        }
    }
}
