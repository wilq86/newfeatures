﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V9_0
{
    [TestFixture]
    public class InitOnlySettersTest
    {
        public struct WeatherObservation
        {
            public DateTime RecordedAt { get; init; }
            public decimal TemperatureInCelsius { get; init; }
            public decimal PressureInMillibars { get; init; }
        }

        [Test]
        public void InitOnlySetters()
        {
            var now = new WeatherObservation
            {
                RecordedAt = DateTime.Now,
                TemperatureInCelsius = 20,
                PressureInMillibars = 998.0m
            };

            //Invalid, Error! CS8852.
            //now.RecordedAt = DateTime.Now;
            Assert.AreEqual(20, now.TemperatureInCelsius);
            Assert.AreEqual(998.0m, now.PressureInMillibars);
        }
    }
}
