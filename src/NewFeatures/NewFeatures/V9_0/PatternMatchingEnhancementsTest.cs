﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V9_0
{
    [TestFixture]
    public class PatternMatchingEnhancementsTest
    {
        public static bool IsLetter(char c) =>
                c is >= 'a' and <= 'z' or >= 'A' and <= 'Z';

        [Test]
        public void PatternMatchingEnhancements()
        {
            char c = 'c';
            Assert.IsTrue(c is >= 'a');
            Assert.IsTrue(c is >= 'a' and <= 'z');
            Assert.IsTrue(c is >= 'a' and <= 'z' or >= 'A' and <= 'Z');

        }

    }
}
