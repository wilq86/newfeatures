﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V9_0
{
    public record Person(string FirstName, string LastName);

    [TestFixture]
    public class RecordsTest
    {
        [Test]
        public void Records() {
            Person person = new Person("Piotr", "Wilk");

            Assert.AreEqual("Piotr", person.FirstName);
            Assert.AreEqual("Wilk", person.LastName);
        }
    }
}
