﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V9_0
{
    [TestFixture]
    public class PartialMethodSyntaxTest
    {
        public partial class PartialClass
        {
            public int Value;
            //Only definition
            partial void Method(int value);

            public void Calculate(int value)
            {
                Method(value);
            }
        }

        public partial class PartialClass
        {
            partial void Method(int value)
            {
                Value = value * 2;
            }
        }

        [Test]
        public void PartialMethodSyntax()
        {
            PartialClass partial = new();

            partial.Calculate(10);
            Assert.AreEqual(20, partial.Value);
        }
    }
}
