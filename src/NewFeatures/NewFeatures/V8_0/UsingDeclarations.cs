﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V8_0
{
    [TestFixture]
    public class UsingDeclarations
    {
        public class Using : IDisposable
        {
            public bool IsDisposed { get; private set; } = false;   

            public void Dispose()
            {
                IsDisposed = true;   
            }
        }

        [Test]
        public void Test()
        {
            Using test;

            {
                using var Using = new Using();
                test = Using;
            }

            Assert.IsTrue(test.IsDisposed);
        }
    }
}
