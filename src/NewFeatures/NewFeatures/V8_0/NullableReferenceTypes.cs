﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V8_0
{
    /// <summary>
    /// https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/nullable-reference-types
    /// </summary>
    [TestFixture]
    public class NullableReferenceTypes
    {
        string? Test = string.Empty;
    }
}
