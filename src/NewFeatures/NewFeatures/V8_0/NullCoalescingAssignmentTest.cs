﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V8_0
{
    [TestFixture]
    public class NullCoalescingAssignmentTest
    {
        [Test]
        public void NullCoalescingAssignment()
        {
            string? value = null;
            value ??= "if diffrent that null";
            Assert.AreEqual("if diffrent that null", value);

            string? value2 = "not null";
            value2 ??= "if diffrent that null";
            Assert.AreEqual("not null", value2);
        }
    }
}
