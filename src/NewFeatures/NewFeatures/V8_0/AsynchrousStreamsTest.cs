﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V8_0
{
    /// <summary>
    /// https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/statements/iteration-statements#await-foreach
    /// </summary>
    [TestFixture]
    public class AsynchrousStreamsTest
    {
        [Test]
        public async Task Test()
        {
            await foreach (var item in GenerateSequenceAsync())
            {
                Console.WriteLine(item);
            }
        }

        private async IAsyncEnumerable<object> GenerateSequenceAsync()
        {
            await Task.Delay(1);
            yield return 1;
            yield return 2;
            yield return 3;
            yield return 4;
        }
    }
}
