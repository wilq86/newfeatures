﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V8_0
{
    [TestFixture]
    public class StaticLocalFunctionsTest
    {
        [Test]
        public void TestStaticLocalFunctions()
        {
            int test = 10;

            int Method()
            {
                //Can interact with test
                return test;    
            }

            static int MyMethod()
            {
                //static Can't interact with test
                //return test;    
                return 0;
            }

            Assert.AreEqual(10, Method()); 
            Assert.AreEqual(0, MyMethod()); 
        }
    }
}
