﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V8_0
{
    [TestFixture]
    public class IndicesAndRanges
    {
        [Test]
        public void Test()
        {
            int[] numbers = new[] { 0, 10, 20, 30, 40, 50 };

            Assert.That(new[] { 10, 20 }, Is.EquivalentTo(numbers[1..3]));
            Assert.That(new[] { 0, 10 }, Is.EquivalentTo(numbers[0..^4]));

            Assert.AreEqual(new[] { 10, 20, 30, 40, 50 }, numbers[1..]);
            Assert.AreEqual(new[] { 0, 10 }, numbers[..2]);

            Range endIndices = ^5..^1;

            Assert.AreEqual(5, endIndices.Start.Value);
            Assert.AreEqual(true, endIndices.Start.IsFromEnd);

            Assert.AreEqual(1, endIndices.End.Value);
            Assert.AreEqual(true, endIndices.End.IsFromEnd);

            //int margin = 1;
            //int[] inner = numbers[margin..^margin];
            //Display(inner);  // output: 10 20 30 40

            //string line = "one two three";
            //int amountToTakeFromEnd = 5;
            //Range endIndices = ^amountToTakeFromEnd..^0;
            //string end = line[endIndices];

        }
    }
}
