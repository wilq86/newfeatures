﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;
using System.Xml.Linq;

namespace NewFeatures.V8_0
{
    [TestFixture]
    public class StringInterpolationTest
    {
        [Test]
        public void StringInterpolation()
        {
            string str = "world";
            Assert.AreEqual("Hello world!", $"Hello {str}!");
        }
    }
}
