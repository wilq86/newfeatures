﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewFeatures.V8_0
{
    public struct ReadOnlyMethodStruct
    {
        public double X;
        public double Y;

        public readonly double Sum()
        {
            return X + Y;
        }
    }
}
