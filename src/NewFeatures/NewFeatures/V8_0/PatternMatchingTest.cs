﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V8_0
{
    [TestFixture]
    public class PatternMatchingTest
    {
        [Test]
        public void DeclarationAndTypePatterns()
        {
            object greeting = "Hello, World!";

            Assert.IsTrue(greeting is string);

            //Assign to variable!
            if (greeting is string message)
            {
                Assert.AreEqual(greeting, message);
            }
        }

        [Test]
        public void TypePatternsBySwitch()
        {
            var numbers = new int[] { 10, 20, 30 };
            Assert.AreEqual(1, GetSourceLabel(numbers));

            var letters = new List<char> { 'a', 'b', 'c', 'd' };
            Assert.AreEqual(2, GetSourceLabel(letters));

            //The run-time type of an expression result is a nullable value type with the underlying type T.
            static int GetSourceLabel<T>(IEnumerable<T> source) => source switch
            {
                Array array => 1,
                ICollection<T> collection => 2,
                _ => 3,
            };
        }

        [Test]
        public void TypePatternsBySwitchUnboxign()
        {
            int? xNullable = 7;
            int y = 23;
            object yBoxed = y;

            if (xNullable is int a && yBoxed is int b)
            {
                Assert.AreEqual(30, a + b); 
            }
        }

        [Test]
        public void PropertyPattern()
        {
            static string TakeFive(object input) => input switch
            {
                string { Length: >= 5 } s => s.Substring(0, 5),
                string s => s,
                _ => throw new ArgumentException("Not supported type")
            };

            Assert.AreEqual("12345", TakeFive("12345abcdef"));
            Assert.AreEqual("1234", TakeFive("1234"));
        }

        public record Point(int X, int Y);
        public record Segment(Point Start, Point End);

        [Test]
        public void PropertyPatternExtended()
        {
            static bool IsAnyEndOnXAxis(Segment segment) =>
                segment is { Start: { Y: 0 } } or { End: { Y: 0 } };

            Assert.AreEqual(true, IsAnyEndOnXAxis(new Segment(new Point(0, 0), new Point(0, 0))));
            Assert.AreEqual(true, IsAnyEndOnXAxis(new Segment(new Point(1, 0), new Point(1, 0))));
            Assert.AreEqual(true, IsAnyEndOnXAxis(new Segment(new Point(0, 1), new Point(0, 0))));
            Assert.AreEqual(true, IsAnyEndOnXAxis(new Segment(new Point(0, 0), new Point(0, 1))));
            Assert.AreEqual(false, IsAnyEndOnXAxis(new Segment(new Point(0, 1), new Point(0, 1))));
        }

        [Test]
        public void Tuple()
        {
            static string sports_i_like(string sport1, string sport2, string sport3)
                    => (sport1, sport2, sport3) switch
                    {
                        //matches if Cricket, Football, and Swimming given as input
                        ("Cricket", "Football", "Swimming") => "I like Cricket, Football, and Swimming.",
                        //matches if Cricket, Football, and Baseball given as input
                        ("Cricket", "Football", "Baseball") => "I like Cricket, Football, and Baseball.",
                        //matches if Hockey, Football, and Swimming given as input
                        ("Hockey", "Football", "Swimming") => "I like Hockey, Football, and Swimming.",
                        //matches if Table Tennis, Football, and Swimming given as input
                        ("Table Tennis", "Football", "Swimming") => "I like Table Tennis, Football and Swimming.",
                        //Default case
                        (_, _, _) => "Invalid input!"
                    };

            Assert.AreEqual("I like Cricket, Football, and Swimming.", sports_i_like("Cricket", "Football", "Swimming"));
            Assert.AreEqual("Invalid input!", sports_i_like("Cricket", "Football", "Racing"));
        }

        public readonly struct PointStruct
        {
            public int X { get; }
            public int Y { get; }

            public PointStruct(int x, int y) => (X, Y) = (x, y);

            public void Deconstruct(out int x, out int y) => (x, y) = (X, Y);
        }

        [Test]
        public void PositionalPattern()
        {
            static string Classify(PointStruct point) => point switch
            {
                (0, 0) => "Origin",
                (1, 0) => "positive X basis end",
                (0, 1) => "positive Y basis end",
                _ => "Just a point",
            };

            Assert.AreEqual("Origin", Classify(new PointStruct(0, 0)));
            Assert.AreEqual("positive X basis end", Classify(new PointStruct(1, 0)));
            Assert.AreEqual("positive Y basis end", Classify(new PointStruct(0, 1)));
            Assert.AreEqual("Just a point", Classify(new PointStruct(100, 100)));
        }
    }
}
