﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V8_0
{
    /// <summary>
    /// Ref Allocated on the stack
    ///    A ref struct can't be the element type of an array.
    /// A ref struct can't be a declared type of a field of a class or a non-ref struct.
    /// A ref struct can't implement interfaces.
    /// A ref struct can't be boxed to System.ValueType or System.Object.
    /// A ref struct can't be a type argument.
    /// A ref struct variable can't be captured by a lambda expression or a local function.
    /// A ref struct variable can't be used in an async method. However, you can use ref struct variables in synchronous methods, for example, in methods that return Task or Task<TResult>.
    /// A ref struct variable can't be used in iterators.
    /// https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/ref-struct
    /// </summary>
    public ref struct CustomRef
    {
        public bool IsValid;
        public Span<int> Inputs;
        public Span<int> Outputs;
    }

    [TestFixture]
    public class RefStructTest
    {
        [Test]
        public void Test()
        {
            //Invalid
            //CustomRef[] array = new CustomRef[1]; 

        }
    }
}
