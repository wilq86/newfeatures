﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace NewFeatures.V8_0
{
    /// <summary>
    /// A type is an unmanaged type if it's any of the following types:
    /// sbyte, byte, short, ushort, int, uint, long, ulong, char, float, double, decimal, or bool
    /// Any enum type
    /// Any pointer type
    /// Any user-defined struct type that contains fields of unmanaged types only.
    /// </summary>
    [TestFixture]
    public class UnmanagedTypesTest
    {
        public struct Coords<T> where T : unmanaged
        {
            public T X;
            public T Y;
        }

        [Test]
        public void UnmanagedTypes()
        {
            Coords<int> integers = new Coords<int>();
            //Invalid
            //Coords<string> strings = new Coords<string>();
            //Coords<object> strings = new Coords<object>();
        }
    }
}
