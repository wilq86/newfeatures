﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V8_0
{
    [TestFixture]
    public class StackallocTest
    {
        int[] variables = new int[10];
        [Test]
        public void Stackalloc() {

            int length = 3;
            Span<int> numbers = stackalloc int[length];
            numbers = variables;
            for (var i = 0; i < length; i++)
            {
                numbers[i] = i;
            }
        }
    }
}
