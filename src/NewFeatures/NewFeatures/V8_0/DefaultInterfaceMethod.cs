﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewFeatures.V8_0
{
    [TestFixture]
    public class DefaultInterfaceMethodTest
    {
        public interface DefaultInterfaceMethod
        {
            bool Method() => false;
        }

        public class DefaultInterfaceMethodClass : DefaultInterfaceMethod
        {

        }

        [Test]
        public void Test()
        {
            DefaultInterfaceMethod method = new DefaultInterfaceMethodClass();

            //Error class must be casted to interface 
            //Assert.AreEqual(false, new DefaultInterfaceMethodClass().Method());
            Assert.AreEqual(false, method.Method());
        }
    }
}
