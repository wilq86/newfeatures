﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V10_0
{
    [TestFixture]
    public class InterpolatedStringHandlerWithParametersTest
    {
        [InterpolatedStringHandler]
        public ref struct LogInterpolatedStringHandler
        {
            // Storage for the built-up string
            StringBuilder builder;

            public string LogLevel { get; }
            public object Source { get; }

            public LogInterpolatedStringHandler(int literalLength, int formattedCount, string logLevel, object source)
            {
                builder = new StringBuilder(literalLength);
                builder.Append($"Source: {source.GetType()}");
                builder.Append($" Level: {logLevel} ");
                LogLevel = logLevel;
                Source = source;
            }

            public void AppendLiteral(string s)
            {
                builder.Append(s);
            }

            public void AppendFormatted<T>(T t)
            {
                builder.Append($"{t.GetType().Name}:{t?.ToString()}");
            }

            internal string GetFormattedText() => builder.ToString();
        }

        public string LogMessage(string level, [InterpolatedStringHandlerArgument("level", "")] LogInterpolatedStringHandler builder)
        {
            return builder.GetFormattedText();
        }

        [Test]
        public void InterpolatedStringHandler()
        {
            int i = 10;
            string str = "A";

            string msg = LogMessage("DEBUG", $"{i.ToString()} {str}");
            Assert.AreEqual("Source: NewFeatures.V10_0.InterpolatedStringHandlerWithParametersTest Level: DEBUG String:10 String:A", msg);
        }
    }
}
