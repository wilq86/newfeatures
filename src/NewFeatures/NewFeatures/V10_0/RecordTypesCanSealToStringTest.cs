﻿using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V10_0
{
    [TestFixture]
    public class RecordTypesCanSealToStringTest
    {

        public record Record
        {
            public string Value { get; set; }   
            public sealed override string ToString() => Value;
        }

        public record ChildRecord : Record
        {

            //Error ToString is sealed
            //public sealed override string ToString() => Value;
        }
    }
}
