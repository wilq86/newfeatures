﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V10_0
{
    

    [TestFixture]
    public class InterpolatedStringHandlerTest
    {
        [InterpolatedStringHandler]
        public ref struct LogInterpolatedStringHandler
        {
            // Storage for the built-up string
            StringBuilder builder;

            public LogInterpolatedStringHandler(int literalLength, int formattedCount)
            {
                builder = new StringBuilder(literalLength);
            }

            public void AppendLiteral(string s)
            {
                builder.Append(s);
            }

            public void AppendFormatted<T>(T t)
            {
                builder.Append($"{t.GetType().Name}:{t?.ToString()}");
            }

            internal string GetFormattedText() => builder.ToString();
        }

        public string LogMessage(LogInterpolatedStringHandler builder)
        {
            return builder.GetFormattedText();
        }

        [Test]
        public void InterpolatedStringHandler()
        {
            int i = 10;
            string str = "A";

            string msg = LogMessage($"{i} {str}");
            Assert.AreEqual("Int32:10 String:A", msg);
        }
    }
}
