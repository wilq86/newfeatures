﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V10_0
{
    [TestFixture]
    public class CallerArgumentExpressionAttributeDiagnosticsTest
    {
        [Test]
        public void CallerArgumentExpressionAttributeDiagnostics()
        {
            string message = Validate(1 == 0);
            Assert.AreEqual("1 == 0", message);
        }

        public static string Validate(bool condition, [CallerArgumentExpression("condition")] string? message = null)
        {
            return message;
        }
    }
}
