﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V10_0
{
    [TestFixture]
    public class ExtendedPropertyPatternsTest
    {
        record Parent(Child Child);

        record Child(string Name);
        

        [Test]
        public void TestExtendedPropertyPatterns()
        {
            Parent parent = new Parent(new Child("Test"));

            Assert.IsTrue(parent is { Child.Name: "Test" });
            Assert.IsTrue(parent is { Child: { Name: "Test" } });
        }
    }
}
