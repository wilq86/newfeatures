﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V10_0
{
    [TestFixture]
    public class ConstantInterpolatedStringsTest
    {

        public const string Key = "key";    
        public const string Value = "Value";    

        public const string InterpolatedConstant = $"{Key}:{Value}";
    }
}
