﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V10_0
{
    public class RecordStructsTest
    {
        public record struct Point
        {
            public double X { get; init; }
            public double Y { get; init; }
            public double Z { get; init; }
        }
    }
}
