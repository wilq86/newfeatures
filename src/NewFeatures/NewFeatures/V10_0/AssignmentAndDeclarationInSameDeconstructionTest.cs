﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewFeatures.V10_0
{
    [TestFixture]
    public class AssignmentAndDeclarationInSameDeconstructionTest
    {
        public record Point(int x, int y);

        [Test]
        public void AssignmentAndDeclarationInSameDeconstruction()
        {
            Point point = new Point(10, 15);

            (int x, int y) = point;

            Assert.AreEqual(10, x);
            Assert.AreEqual(15, y);
        }
    }
}
